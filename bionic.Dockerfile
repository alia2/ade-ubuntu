# An ADE-compatible image providing an Ubuntu development environment.

FROM ubuntu:bionic



ENTRYPOINT ["/ade_entrypoint"]
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & while wait ${!}; [ $? -ge 128 ]; do true; done"]


# Unminimize Ubuntu image: get manpages back and stuff
# https://github.com/tianon/docker-brew-ubuntu-core/issues/122#issuecomment-436348800
RUN yes | unminimize              \
    && apt-get install -y         \
    man-db                        \
    && rm -r /var/lib/apt/lists/* \
    # This is a really useless 75Mb
    # https://unix.stackexchange.com/questions/180400/is-it-safe-to-empty-usr-share-doc
    && rm -rf /usr/share/doc


ENV TZ=Europe/Paris
# Because it is used in entrypoint (but I don't understand what it does)
ENV TIMEZONE=Europe/Paris


# TODO: understand what is done here, maybe cleanup what is done in adeinit
RUN locale-gen en_US en_US.UTF-8
RUN dpkg-reconfigure -f noninteractive locales
RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8


# Allow sudo without password
RUN echo 'ALL ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers


# Dependencies for X11 applications
RUN apt-get update && apt-get -y install \
    fontconfig                           \
    libgdk-pixbuf2.0-0                   \
    libgmp10                             \
    libpango-1.0                         \
    libpangocairo-1.0                    \
    x11-utils                            \
    && rm -rf /var/lib/apt/lists/*


# Basic tools for developers
RUN apt-get update && apt-get -y install \
    build-essential                      \
    curl                                 \
    git                                  \
    htop                                 \
    tree                                 \
    vim                                  \
    wget                                 \
    zsh                                  \
    && rm -rf /var/lib/apt/lists/*


##
## Copy local files into the system
##

COPY skel/. /
