# An ADE-compatible image providing an Ubuntu development environment with extra dev tools.

ARG FROM_IMAGE=registry.gitlab.com/deb0ch/ade-ubuntu/bionic:latest

FROM $FROM_IMAGE



# Install oh-my-zsh system-wide
# https://stackoverflow.com/questions/31624649/how-can-i-get-a-secure-system-wide-oh-my-zsh-configuration
RUN apt-get update && apt-get -y install \
    git                                  \
    && rm -rf /var/lib/apt/lists/*
RUN git clone                                                                              \
        -c core.eol=lf                                                                     \
        -c core.autocrlf=false                                                             \
        -c fsck.zeroPaddedFilemode=ignore                                                  \
        -c fetch.fsck.zeroPaddedFilemode=ignore                                            \
        -c receive.fsck.zeroPaddedFilemode=ignore                                          \
        --depth=1                                                                          \
        --branch master                                                                    \
        https://github.com/ohmyzsh/ohmyzsh.git                                             \
        /usr/share/oh-my-zsh                                                               \
    # Setting the default shell for new users has no effect since:
    # 1. The default shell is specified when creating new users in entrypoint
    # 2. The `ade enter` command will execute `bash` anyways
    && sed -i 's/DSHELL=\/bin\/bash/DSHELL=\/bin\/zsh/g' /etc/adduser.conf

# Fix for zsh compinit error at startup
RUN chmod g-w,o-w /usr/local/share/zsh/site-functions


# Steps (https://github.com/deb0ch/steps)
COPY tools/steps /usr/bin/steps


# Ripgrep-All https://github.com/phiresky/ripgrep-all
RUN curl -LO https://github.com/BurntSushi/ripgrep/releases/download/12.1.0/ripgrep_12.1.0_amd64.deb \
    && dpkg -i ripgrep_12.1.0_amd64.deb                                                              \
    && rm ripgrep_12.1.0_amd64.deb

RUN apt-get update && apt-get -y install \
    pandoc                               \
    poppler-utils                        \
    ffmpeg                               \
    cargo                                \
    && rm -rf /var/lib/apt/lists/*

RUN curl -LO https://github.com/phiresky/ripgrep-all/releases/download/v0.9.6/ripgrep_all-v0.9.6-x86_64-unknown-linux-musl.tar.gz \
    && tar -xzvf ripgrep_all-v0.9.6-x86_64-unknown-linux-musl.tar.gz                                                              \
    && mv ripgrep_all-v0.9.6-x86_64-unknown-linux-musl/rga* /usr/bin                                                              \
    && rm -rf ripgrep_all-v0.9.6-x86_64-unknown-linux-musl*


# ADE volume dependencies

# Dependencies for ade-atom
RUN apt-get update && apt-get -y install \
    libgtk-3-0                           \
    x11-utils                            \
    libxss1                              \
    gconf2                               \
    libnss3                              \
    libasound2                           \
    libxkbfile1                          \
    && rm -rf /var/lib/apt/lists/*
